var fs = require('fs');

const RESET_PIN = (4-1)*32+24;
const SDSEL_PIN = 0;
const execSync = require('child_process').execSync;

function unexportPin(pinnum){
	if(fs.existsSync('/sys/class/gpio/gpio'+pinnum.toString())){
		fs.writeFileSync('/sys/class/gpio/unexport', pinnum.toString());
	}	
}

function exportPin(pinnum){
	fs.writeFileSync('/sys/class/gpio/export', pinnum.toString());
	fs.writeFileSync('/sys/class/gpio/gpio'+pinnum.toString()+'/direction', 'out');
}

function setPin(pinnum, val){
	fs.writeFileSync('/sys/class/gpio/gpio'+pinnum.toString()+'/value', val.toString());
}

function waitSync(ms){
	var start = new Date().getTime();
	while(new Date().getTime() < start + ms){;}
}

exports.inithardware = function(){

	unexportPin(RESET_PIN);
	unexportPin(SDSEL_PIN);
	waitSync(10);

	exportPin(SDSEL_PIN);
	waitSync(10);
	setPin(SDSEL_PIN, 0);
	// Clean log files
	execSync('rm -rf /tmp/logsrv');

	// Eject SD card
	if(!fs.existsSync('/dev/mmcblk0p1')){
		console.log('[utils] There is no SD card!');
		return 0;
	}

	// Reset MCU
	console.log('[utils] Reset MCU ...')
	exportPin(RESET_PIN);
	setPin(RESET_PIN, 0);
	waitSync(10);
	setPin(RESET_PIN, 1);
	waitSync(10);

	return 1;
}

exports.start = function(){
	console.log('[utils] Ejecting SD card...')
	execSync('sudo umount /run/media/mmcblk0p1 || true');

	waitSync(200);
	setPin(SDSEL_PIN, 1);
	waitSync(1000);
	console.log('[utils] Ejecting SD card finish!');
}

function makeFileName(d){
	var fileName = '';
	for(var key in d){
		if(key != 'state'){
			fileName = fileName + d[key].toString() + key + '_';
		}
	}

	return fileName.replace(/\./gi, '_').toUpperCase();
}

Date.prototype.mydateformat = function() {
  var yy = this.getFullYear()%1000;
  var mm = this.getMonth() + 1;
  var dd = this.getDate();
  var hh = this.getHours();
  var min = this.getMinutes();

  return [yy,
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd,
          (hh>9 ? '' : '0') + hh,
          (min>9 ? '' : '0') + min
         ].join('');
};

exports.finish = function(d){
	// insert SD card
	console.log('[utils] Insert SD card...')
	setPin(SDSEL_PIN, 0);
	waitSync(4000);
	//execSync('sudo mount -t vfat /dev/mmcblk0p1 /run/media/mmcblk0p1 || true');
	//execSync('sync');

	fs.readdir('/run/media/mmcblk0p1', (err, filelist)=>{
			if(filelist.indexOf('LOG.BIN') > -1){
				//var currentTime = new Date().toISOString();
				var dd = new Date();
				var fileName = makeFileName(d) + dd.mydateformat();
				console.log(fileName);

				// Copy file to system
				console.log('[utils] Copy logging file...');
				execSync('mkdir -p /tmp/logsrv');
				execSync('cp /run/media/mmcblk0p1/LOG.BIN /tmp/logsrv/' + fileName + '.bin');
				console.log('[utils] Finish!');
			}else{
				console.log('[utils] No logging files!');
			}
	});
}

exports.parseData = function(d){
	if(d.length == 5){
		state = d[0];
		temp = d[1];
		//temp = new Float32Array(new Uint8Array([ d[1], d[2], d[3], d[4]]).buffer)[0]
		return {'temp':temp, 'state':state}
	}

	return {}
}

exports.dataToBytes = function(d){
	var buffer = new ArrayBuffer(25)
	var uint8View = new Uint8Array(buffer);
	uint8View[0] = 0xAA;
	uint8View[1] = 0xAA;
	uint8View[23] = 0xAA;
	uint8View[24] = 0xAA;

	var d_array = new Uint8Array(new Float32Array([d['Vdc'], d['Vp'], d['f'], d['t'], d['phi'] ]).buffer)
	for(var i = 0;i<20;i++){
		uint8View[i+2] = d_array[i];
	}

	uint8View[22] = d['state'];

	return uint8View;
}