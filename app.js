var fs = require('fs');
var utils = require('./utils.js');
var d_last = {};

// Init hardware
if(!utils.inithardware()){
	console.log('[server] Hardware initialize faile...');
	return 0;
}

process.on('SIGINT', function(){
	utils.finish();
	process.exit();
});

// serialport
const SerialPort = require('serialport');
const port = new SerialPort('/dev/ttymxc4', { baudRate : 115200 });

port.on('error', function(err){
	console.log('[serial] ' + err);
});

// State manager
var StateManager = new require('./sman.js');
var sman = new StateManager();

// Express
var express = require('express');
var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(express.static('public'));
app.use('/log', express.static('/tmp/logsrv'));

app.get('/', function(req,res){
	res.render('index');
});

var http = require('http').createServer(app);
http.listen(3000, function(){
	console.log('[server] listening on *:3000');
});

// Socket.io Settings
var io = require('socket.io')(http);
io.on('connection', (socket) => {
	console.log('[server] a user connected');

	socket.on('refresh', (msg) => {
		fs.readdir('/tmp/logsrv', (err, filelist)=>{
			socket.emit('filelist', filelist);
		});
	});

	socket.on('control', (msg) => {
		sman.userRequest(msg);
	});

	socket.on('disconnect', function(){
		console.log('[server] user disconnected');
	});
});

port.on('data', (data)=>{
	d = utils.parseData(data);
	sman.mcuRequest(d);
});

sman.on('mcu_action', function(msg){;
	d_last = msg;
	port.write(utils.dataToBytes(msg));
});

sman.on('user_action', function(msg){
	io.emit('state', msg);
});

sman.on('system_action', function(msg){
	switch(msg){
		case 'eject_sd': utils.start(); break;
		case 'insert_sd': utils.finish(d_last); break;
	}
});