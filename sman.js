const LOG_IDLE = 0;
const LOG_ACTIVE = 1;
const LOG_ERR = 2;

module.exports = class StateManager {

	constructor(){
		this.mcu_state = 0;
		this.user_state = 0;
		this.sd_state = 1;
	}

	userRequest(d){
		this.user_state = d.state;

		if(this.user_state == LOG_ACTIVE && this.mcu_state != LOG_ACTIVE && this.sd_state == 1){
			this.system_action('eject_sd');
			this.sd_state = 0;
		}
			
		this.mcu_action(d);
	} 

	mcuRequest(d){
		if(d.state == undefined) return;
		if(this.mcu_state != d.state){
			if(d.state != LOG_ACTIVE && this.sd_state == 0){
			this.system_action('insert_sd');
			this.sd_state = 1;
			}
		}
		this.mcu_state = d.state;
		this.user_action(d);
	}

	on(c, callback){
		switch(c){
			case 'mcu_action':
			this.mcu_action = callback;
			break;

			case 'user_action':
			this.user_action = callback;
			break;

			case 'system_action':
			this.system_action = callback;
		}
	}
}